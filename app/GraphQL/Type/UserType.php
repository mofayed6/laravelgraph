<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;

class UserType extends BaseType
{
    protected $attributes = [
        'name' => 'UserType',
        'description' => 'A type'
    ];

    public function fields()
    {
        return [
            'id'=>[
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of a user'
            ],
            'name'=>[
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of a user'
            ],
            'email'=>[
                'type' => Type::nonNull(Type::string()),
                'description' => 'The email of a user'
            ],
            'bits'=>[
                'type' => Type::listOf(GraphQL::type('Bit')),
                'description' => 'The email of a user'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Date a was created'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Date a was updated'
            ],
        ];
    }
}
